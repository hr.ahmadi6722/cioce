<!-- CioCe Products -->
<div class="container mt-3">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 my-3">
            <h2 class="title-default green"><span>پیشنهاد فوق العاده</span></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="owl-container">
                <div class="owl-carousel owl-theme owl">
                    <a href="#">
                        <div class="slider-desc text-center overflow-hidden">
                            <div class="item">
                                <div style="width: 200px;" class="row justify-content-center align-items-center overflow-hidden">
                                    <img style="max-height: 400px" src="{{url('/images/product/cioce_product/website-template-60525.jpg')}}"
                                         alt="CioCe.ir" class="img-fluid">
                                </div>
                                <div class="price-box rtl">
                                    <p class="font-14 pb-2 border-bottom nowrap font-weight-bold">Hipi Style</p>
                                    <div>
                                        <del class="font-14 nowrap text-secondary">2,000,000 <span
                                                    class="font-12">تومان</span></del>
                                        <span class="badge badge-danger font-14 mt-1">20<span>%</span></span>
                                        <p class="text-danger font-18 mt-1 nowrap">1,800,000<span
                                                    class="font-12">تومان</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#">
                        <div class="slider-desc text-center overflow-hidden">
                            <div class="item">
                                <div style="width: 200px;" class="row justify-content-center align-items-center overflow-hidden">
                                    <img style="max-height: 400px" src="{{url('/images/product/cioce_product/website-template-63541.jpg')}}"
                                         alt="CioCe.ir" class="img-fluid">
                                </div>
                                <div class="price-box rtl">
                                    <p class="font-14 pb-2 border-bottom nowrap font-weight-bold">Hipi Style</p>
                                    <div>
                                        <del class="font-14 nowrap text-secondary">2,000,000 <span
                                                    class="font-12">تومان</span></del>
                                        <span class="badge badge-danger font-14 mt-1">20<span>%</span></span>
                                        <p class="text-danger font-18 mt-1 nowrap">1,800,000<span
                                                    class="font-12">تومان</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#">
                        <div class="slider-desc text-center overflow-hidden">
                            <div class="item">
                                <div style="width: 200px;;" class="row justify-content-center align-items-center overflow-hidden">
                                    <img style="max-height: 400px" src="{{url('/images/product/cioce_product/website-template-82540.jpg')}}"
                                         alt="CioCe.ir" class="img-fluid">
                                </div>
                                <div class="price-box rtl">
                                    <p class="font-14 pb-2 border-bottom nowrap font-weight-bold">Hipi Style</p>
                                    <div>
                                        <del class="font-14 nowrap text-secondary">2,000,000 <span
                                                    class="font-12">تومان</span></del>
                                        <span class="badge badge-danger font-14 mt-1">20<span>%</span></span>
                                        <p class="text-danger font-18 mt-1 nowrap">1,800,000<span
                                                    class="font-12">تومان</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#">
                        <div class="slider-desc text-center overflow-hidden">
                            <div class="item">
                                <div style="width: 200px;;" class="row justify-content-center align-items-center overflow-hidden">
                                    <img style="max-height: 400px" src="{{url('/images/product/cioce_product/website-template-23291.jpg')}}"
                                         alt="CioCe.ir" class="img-fluid">
                                </div>
                                <div class="price-box rtl">
                                    <p class="font-14 pb-2 border-bottom nowrap font-weight-bold">Hipi Style</p>
                                    <div>
                                        <del class="font-14 nowrap text-secondary">2,000,000 <span
                                                    class="font-12">تومان</span></del>
                                        <span class="badge badge-danger font-14 mt-1">20<span>%</span></span>
                                        <p class="text-danger font-18 mt-1 nowrap">1,800,000<span
                                                    class="font-12">تومان</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>


                    <a href="#">
                        <div class="slider-desc text-center overflow-hidden">
                            <div class="item">
                                <div style="width: 200px;;" class="row justify-content-center align-items-center overflow-hidden">
                                    <img style="max-height: 400px" src="{{url('/images/product/cioce_product/website-template-24984.jpg')}}"
                                         alt="CioCe.ir" class="img-fluid">
                                </div>
                                <div class="price-box rtl">
                                    <p class="font-14 pb-2 border-bottom nowrap font-weight-bold">Hipi Style</p>
                                    <div>
                                        <del class="font-14 nowrap text-secondary">2,000,000 <span
                                                    class="font-12">تومان</span></del>
                                        <span class="badge badge-danger font-14 mt-1">20<span>%</span></span>
                                        <p class="text-danger font-18 mt-1 nowrap">1,800,000<span
                                                    class="font-12">تومان</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#">
                        <div class="slider-desc text-center overflow-hidden">
                            <div class="item">
                                <div style="width: 200px;;" class="row justify-content-center align-items-center overflow-hidden">
                                    <img style="max-height: 400px" src="{{url('/images/product/cioce_product/website-template-16742.jpg')}}"
                                         alt="CioCe.ir" class="img-fluid">
                                </div>
                                <div class="price-box rtl">
                                    <p class="font-14 pb-2 border-bottom nowrap font-weight-bold">Hipi Style</p>
                                    <div>
                                        <del class="font-14 nowrap text-secondary">2,000,000 <span
                                                    class="font-12">تومان</span></del>
                                        <span class="badge badge-danger font-14 mt-1">20<span>%</span></span>
                                        <p class="text-danger font-18 mt-1 nowrap">1,800,000<span
                                                    class="font-12">تومان</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#">
                        <div class="slider-desc text-center overflow-hidden">
                            <div class="item">
                                <div style="width: 200px;" class="row justify-content-center align-items-center overflow-hidden">
                                    <img style="max-height: 400px" src="{{url('/images/product/cioce_product/website-template-23291.jpg')}}"
                                         alt="CioCe.ir" class="img-fluid">
                                </div>
                            </div>
                            <div class="price-box rtl">
                                    <p class="font-14 pb-2 border-bottom nowrap font-weight-bold">Hipi Style</p>
                                    <div>
                                        <del class="font-14 nowrap text-secondary">2,000,000 <span
                                                    class="font-12">تومان</span></del>
                                        <span class="badge badge-danger font-14 mt-1">20<span>%</span></span>
                                        <p class="text-danger font-18 mt-1 nowrap">1,800,000<span
                                                    class="font-12">تومان</span></p>
                                    </div>
                                </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
