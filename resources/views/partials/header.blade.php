<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-white py-2 px-0 ltr">
        <a href="{{route('home')}}" class="navbar-brand">
            <img class="img-fluid" style="max-width: 89px;" src="/images/logo/cioce-tet-logo.png" alt="CioCe">
        </a>
        <a href="{{route('cart_page')}}" class="m-0 p-0" title="سبد خرید"><i class="fas fa-cart-plus font-20 text-primary pt-1"></i>
            @if(!empty(Illuminate\Support\Facades\Session::has('product') && count(Illuminate\Support\Facades\Session::get('product')) > 0))
                <span class="badge font-11 badge-danger text-white m-0 p-1">
                    {{count(Illuminate\Support\Facades\Session::get('product'))}}
                </span>
            @endif
        </a>
        <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"> <span class="navbar-toggler-icon"></span> </button>
        <div id="navbarContent" class="collapse navbar-collapse mt-3 mt-lg-0 rtl">
            <ul class="navbar-nav pr-0 ml-auto">
                <li class="nav-item"><a href="/" class="nav-link px-3 pr-0"><i class="fa fa-home font-19 fa-lg d-none d-sm-inline pl-2 text-primary"></i>صفحه اصلی</a></li>
                <li class="nav-item dropdown megamenu"><a id="megamneu" href="" data-toggle="dropdown" class="nav-link dropdown-toggle px-3">دسته بندی</a>
                    <div aria-labelledby="megamneu" class="dropdown-menu border-0 p-0 m-0">
                        <div class="container">
                            <div class="row w-100 bg-white rounded-0 m-0 mt-1 shadow">
                                <div class="col-lg-12">
                                    <div class="p-4">
                                        <div class="row">
                                            <div class="col-sm-6 col-lg-3 mb-3 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 1</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-lg-3 mb-3 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 2</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-lg-3 mb-3 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 3</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-lg-3 mb-3 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 4</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-lg-3 mb-3 mb-lg-0 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 5</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-lg-3 mb-3 mb-lg-0 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 6</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-lg-3 mb-3 mb-lg-0 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 7</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-lg-3 mb-3 mb-lg-0 text-right">
                                                <h6 class="font-weight-bold font-13 text-uppercase">دسته بندی 8</h6>
                                                <ul class="list-unstyled pr-0">
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                    <li class="nav-item"><a href="" class="nav-link font-14 pt-0 pb-0">زیر دسته</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item"><a href="{{ route("About_Us") }}" class="nav-link px-3">درباره ما</a></li>
                <li class="nav-item"><a href="{{ route("contact") }}" class="nav-link px-3">تماس با ما</a></li>
                @if(!Auth::check())
                    <a href="#" class="nav-link px-3 text-primary" data-toggle="modal" data-target="#login-register">ورود / ثبت نام</a>
                @else
                    <div class="btn-group nav-link" style="cursor: pointer">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            @if(Auth::check() && Auth::user()->user_mode == "gold")
                                <span class="fa fa-diamond font-16 text-warning pl-1"></span>
                            @endif
                            @if(Auth::user()->name == "")
                                {{ Auth::user()->mobile }}
                            @elseif(Auth::user()->name !== "")
                                {{ Auth::user()->name }}
                            @endif
                        </a>
                        <div class="dropdown-menu dropdown-menu-right text-right font-13">
                            <a class="dropdown-item px-3"> اعتبار {{ number_format(Auth::user()->credit) }} ریال </a>
                            @if(Auth::check() && Auth::user()->role == "admin")
                                <a href="{{ route("cioce") }}" class="dropdown-item px-3">پنل مدیریت</a>
                            @endif
                            <a href="{{ route("profile_index") }}" class="dropdown-item px-3">پروفایل</a>
                            <a href="{{ route("logout") }}" class="dropdown-item px-3">خروج</a>
                        </div>
                    </div>
                @endif
                <li class="nav-item"><a data-toggle="modal" data-target="#search-modal" href="#" class="nav-link px-3"><span class="fa fa-search text-primary font-15 pl-1"></span>جستجو </a></li>
            </ul>
        </div>
    </nav>
</div>



{{--
<header class="site-navbar js-sticky-header site-navbar-target ltr" role="banner">
    <div class="container">
        <div class="row align-items-center">


            <div class="col-4 col-md-8 d-none d-xl-block">
                <nav class="site-navigation position-relative text-right" role="navigation">
                    <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">

                        @if(!Auth::check())
                            <li><a href="#" class="nav-link text-primary" data-toggle="modal"
                                   data-target="#login-register">ورود / ثبت نام</a></li>
                        @else
                            <li class="has-children">
                                @if(Auth::check() && Auth::user()->user_mode == "gold")
                                    <a href="#profile" class="nav-link text-warning">
                                        @if(Auth::user()->name == "")
                                            {{ Auth::user()->mobile }}
                                        @elseif(Auth::user()->name !== "")
                                            {{ Auth::user()->name }}
                                        @endif
                                        <span class="fa fa-diamond font-16 text-warning pl-1"></span></a>
                                @else
                                    <a href="#profile"
                                       class="nav-link">
                                        @if(Auth::user()->name == "")
                                            {{ Auth::user()->mobile }}
                                        @elseif(Auth::user()->name !== "")
                                            {{ Auth::user()->name }}
                                        @endif
                                    </a>
                                @endif
                                <ul class="dropdown text-right rtl">
                                    <li><a class="nav-link text-success">
                                            اعتبار {{ number_format(Auth::user()->credit) }} ریال </a></li>
                                    @if(Auth::check() && Auth::user()->role == "admin")
                                        <li><a href="{{ route("cioce") }}" class="nav-link">پنل مدیریت</a></li>
                                    @endif
                                    <li><a href="{{ route("profile_index") }}" class="nav-link">پروفایل</a></li>
                                    <li><a href="{{ route("logout") }}" class="nav-link">خروج</a></li>
                                </ul>
                            </li>
                        @endif
                        <li><a href="{{ route("contact") }}" class="nav-link">تماس با ما</a></li>
                        <li><a href="{{ route("About_Us") }}" class="nav-link">درباره ما</a></li>
                        <li class="has-children"><a href="#contact-section" class="nav-link">مرکز خدمات</a>
                            <ul class="dropdown text-right">
                                <li><a href="#" class="nav-link">دریافت ثمین کارت</a></li>
                                <li><a href="#" class="nav-link">هواداران استقلال</a></li>
                                <li><a href="#" class="nav-link">جشنواره</a></li>
                                <li><a href="#" class="nav-link">مطب</a></li>
                                <li><a href="#" class="nav-link">قرعه کشی</a></li>
                                <li><a href="#" class="nav-link">خبرنامه</a></li>
                                <li><a href="#" class="nav-link">باشگاه مشتریان</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
    </div>
</header>
--}}


