<footer class="container-fluid ltr site-footer mt-3">
    <div class="container">
        <div class="row">
            <div class="rtl col-md-3 myfont">
                <h2 class="footer-heading mb-4 text-center">درباره ما</h2>
                <p class="text-right">فروشگاه CioCe در سال 1396 کار خود را شروع کرد و در طول این چند سال
                    پیشرفت های بزرگی در صنعت فروشگاه های آنلاین داشته است.</p>
            </div>
            <div class="col-md-3 mr-auto text-center myfont">
                <h2 class="footer-heading mb-4">لینک های مرتبط</h2>
                <ul class="list-unstyled">
                    <li><a href="#about-section" class="smoothscroll text-decoration-none">قوانین</a></li>
                    <li><a href="#about-section" class="smoothscroll text-decoration-none">تماس با ما</a></li>
                    <li><a href="#about-section" class="smoothscroll text-decoration-none">کاربر طلایی</a></li>
                    <li><a href="#contact-section" class="smoothscroll text-decoration-none">تماس با ما</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 footer-social text-center">
                <h2 class="footer-heading mb-4">Follow Us</h2>
                <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="fa fa-instagram"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </div>
            <div class="col-md-3 text-center">
                <h2 class="footer-heading mb-4">عضو خبرنامه شوید</h2>
                <form action="#" method="post" class="footer-subscribe">
                    <div class="input-group mb-3">
                        <input type="text" style="height: 43px" class="form-control border-secondary text-white bg-transparent"
                               placeholder="رایانامه" aria-label="Enter Email" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button" id="button-addon2">عضویت
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12">
                <div class="border-top pt-5">
                    <p class="copyright">
{{--                        <small>--}}
{{--                            Copyright &copy;<script>document.write(new Date().getFullYear());</script>--}}
{{--                            تمامی حقوق این سایت متعلق به CioCe.ir می باشد <i--}}
{{--                                class="fa fa-exclamation-triangle text-primary"--}}
{{--                                aria-hidden="true"></i> by <a--}}
{{--                                href="https://CioCe.ir" target="_blank">www.CioCe.ir</a>--}}
{{--                        </small>--}}
                        <small>
                            <p class="p-0 m-0">تمامی حقوق این وب سایت متعلق به فروشگاه اینترنتی  سیو سه می باشد</p>
                            <p class="p-0 m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script> / <a href="www.cioce.ir">www.cioce.ir</a></p>
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
